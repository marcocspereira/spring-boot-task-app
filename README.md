# Task Application

This repository has a set of projects based on a task manager. The main goal is to practice development skills with a stack based on Spring Boot (Java) for backend + Angular framework for frontend. Three alternatives are presented:

1. Building a Simple Tasks Application with Angular and Spring Boot;
2. JHipster > Angular + Spring Boot Tasks Application;
3. JHipster > JBlog App (bonus)

This was inspired in [Angular 4 Java Developers](https://www.udemy.com/angular-4-java-developers/) course @ [Udemy](https://www.udemy.com/), created by [Dan Vega](https://www.udemy.com/user/danvega/), [John Thompson](https://www.udemy.com/user/john-thompson-2/).

## JHipster - Angular + Spring Boot Tasks Application

### Development Environment

* Java 8;
* Maven;
* NodeJS 10;
* Yarn;

### How to generate a JHipster app

Afer install jhipster (``npm install -g generator-jhipster``):

* mkdir jhipster-tasls
* jhipster
* Answer the questions:
    
    * Monolithic, since this is a simple project;
    * name the application as ``tasks``;
    * the package name could be ``com.marcocspereira``;
    * no need to use JHipster Registry;
    * JWT that give us tokens for our REST API;
    * Select a database of your preference for production;
    * Select an in-memory for local development, like H2;
    * Cache: Yes, with Ehcache because we one node. If you were building out a micro-service architecture with a distributed cache, Hazelcast may be the answer;
    * Select Maven or Gradle;
    * No other technologies;
    * Framework to use for the client: Angular;
    * Now, is ``no`` until the end.

Now wait :)

* To run Spring Boot Application: ``./mnvw``;

* To start webpack development server ``yarn start``.