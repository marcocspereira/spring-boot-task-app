/**
 * View Models used by Spring MVC REST controllers.
 */
package com.marcocspereira.web.rest.vm;
