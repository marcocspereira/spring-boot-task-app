package com.marcocspereira.task.service;

import com.marcocspereira.task.domain.Task;

public interface TaskService {

    Iterable<Task> list();

    Task save(Task task);
}
