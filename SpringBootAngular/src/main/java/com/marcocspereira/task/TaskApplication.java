package com.marcocspereira.task;

import com.marcocspereira.task.domain.Task;
import com.marcocspereira.task.service.TaskService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@SpringBootApplication
public class TaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskApplication.class, args);
    }

    // using command line runner
    @Bean
    CommandLineRunner runner(TaskService taskService) {
        return args -> {
            taskService.save( new Task("Create Spring Boot Application", LocalDate.now(), true) );
            taskService.save( new Task("Create Spring Boot Project Packages", LocalDate.now().plus(1, ChronoUnit.DAYS), false) );
            taskService.save( new Task("Create the Task Domain Class", LocalDate.now().plus(2, ChronoUnit.DAYS), true) );

        };
    }

}

