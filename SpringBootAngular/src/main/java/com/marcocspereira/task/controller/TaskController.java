package com.marcocspereira.task.controller;

import com.marcocspereira.task.domain.Task;
import com.marcocspereira.task.service.TaskService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    // get any of the data that is in our database
    @GetMapping( value = {"", "/"})
    public Iterable<Task> list() {
        // reach out our repository
        return this.taskService.list();
    }

    // to save a task from the Angular side
    // @RequestBody -> this is basically what we're posting from Angular to Spring Application
    @PostMapping( value = {"/save"})
    public Task saveTask(@RequestBody Task task) {
        return this.taskService.save(task);
    }

    @PostMapping( value = {"/add"})
    public Task addTask(@RequestBody Task task) { return this.taskService.save(task);

    }

}
