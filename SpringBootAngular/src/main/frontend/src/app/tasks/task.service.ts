import {HttpClient} from "@angular/common/http";
import {EventEmitter, Injectable} from "@angular/core";
import {Task} from "./task.model";

@Injectable()
export class TaskService {

  onTaskAdded = new EventEmitter<Task>()

  constructor(private httpClient: HttpClient) {

  }

  /**
   * Reach out to Spring Boot
   * and get the list of tasks.
   */

  getTasks() {
    return this.httpClient.get<Task[]>('/api/tasks');
  }

  /**
   * Reach out to Spring Boot
   * and do something.
   * @param task
   * @param checked
   */

  saveTask(task: Task, checked: boolean) {
    task.completed = checked;
    return this.httpClient.post('api/tasks/save', task);
  }

  addTask(task: Task) {
    return this.httpClient.post('api/tasks/add', task);
  }

}
