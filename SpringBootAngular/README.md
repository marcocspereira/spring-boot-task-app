# README

This project is created as a Spring Boot application + Angular application. This is very important to understand what is happening behind the scenes of *JHipster*.

## Spring Boot (Java)

The *main* method should be writing the Spring Boot Application class. This class should be annotated with **SpringBootApplication**. This is the entry point of the spring boot application to start.

In this process, the main class is located at the **src/main/java/com.marcocspereira.task/TaskApplication**. 

### A Rest endpoint

* Adding a *rest controller* inside *controller* package, annotated with
**RestController**.
* The *Request URI* method is defined with the **RequestMapping** annotation.
* Then, the *Request URI* method should return a string.

This project has **3 endpoints**:

* To list tasks;
* To update a task;
* To add a task.

### The domain package

This package has the classes that represent each entity to persist.

Must have annotations:

* **@Entity**
* **@Data**
* **@AllArgsConstructor**

### The Spring Data repository abstraction 
 
The goal of Spring Data repository abstraction is to significantly reduce the amount of boilerplate code required to **implement data access layers for various persistence stores**.

Some functionalities:

* Saves the given entity.
* Returns the entity identified by the given id.
* Returns all entities.
* Returns the number of entities.
* Deletes the given entity.
* Indicates whether an entity with the given id exists.

More info, please check https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html.


### Overview

When we hit ``localhost:8080/api/tasks/``, we're actually calling a method called list, that's gonna reach out our **task service**, which handles the business logic.

It passes that responsability on to the **repository** for our *persistence* layer and it's going to return to us in a *Iterable* list of taks.

---

## Angular

Frontend app.

### Components

* tasks
* tasks-list
* tasks-add

These two are children from the first one.

### Services

* taskService

It allows to communicate with our backend app (Spring Boot) to get available tasks and create and edit tasks.

### Models

* service

It supports the information that comes from backend into a Service type.



### Packaging up Angular app into Spring Boot app (extra-step)

In order to have a single app, that we can could take and create a *.jar* or a *.war* and deploy somewhere else, we build the *Angular* app, which then calls webpack to bundle up all the assets and push them into the resources static directory of the *Spring Boot* app, so we can just run the *Spring Boot* app and test the application.
 

* Install *npm* dependencies (*npm install --save-dev <package>*):
    * **rimraf:** is going to look at a directory and then make a new directory. This is basically removing the directory and create the new one. It is similar to the *UNIX* command *rm -rf*;
    * **mkdirp:** recursively mkdir, like `mkdir -p`, but in node.js;
    * **copyfiles:** copy files easily.
    
* Add some new scripts in *package.json*:
```json
{
  "scripts": {
    "postbuild": "npm run deploy",
    "deploy": "copyfiles -f dist/** ../resources/static",
    "predeploy": "rimraf ../resources/static && mkdirp ../resources/static"
  }
}
```
When we **build** something, we can do a **post build** that runs after we build.
Before we **deploy**, we can run a **pre deploy** that removes the directory *../resources/static/* and then make the directory. Essentially, it's just clearing it out.

Once we cleared out, we want to copy the files from the distribution folder (*dist/***), which is where *Angular* places everything, into our static folder (*../resources/static*).

* Run **build**: *npm run build* that calls:
    1. ng build;
    2. npm run deploy (called with *postbuild*);
    3. rimraf ../resources/static/ && mkdirp ../resources/static (calle with *predeploy*);
    4. copyfiles -f dist/** ../resources/static.
    
* Well, for some reason, at the moment, the resources folder is still empty :(